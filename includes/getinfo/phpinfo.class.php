<?php
class phpinfo
{
	public static function isAvailable()
	{
		return true;
	}
	
	public static function getInfo()
	{
		$result = [
			'version' => phpversion(),
			#'ini' => ini_get_all(),
			
			'scriptowner' => get_current_user(),
			'uname' => php_uname(),
			'sapi' => php_sapi_name(),
			'dir' => __DIR__,
			#'scriptexecuter' => posix_getpwuid(posix_geteuid()),
			'extensions' => [],
			'ini' => [],
		];
		
		$ini = ['max_execution_time', 'max_input_time', 'ignore_user_abort', 'memory_limit', 'allow_url_fopen', 'post_max_size', 'upload_max_filesize', 'disable_functions', 'open_basedir'];
		sort($ini);
		foreach($ini as $setting)
			$result['ini'][$setting] = ini_get($setting);
			
        foreach(get_loaded_extensions() as $ext)
            $result['extensions'][$ext] = phpversion($ext);
			
		return $result;
	}
}
?>
