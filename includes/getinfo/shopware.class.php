<?php

class shopware
{
    protected static $contao56File;
	public static function isAvailable()
	{
        self::$contao56File = $_SERVER['DOCUMENT_ROOT'] . '/../engine/Shopware/Application.php';
		if (file_exists(self::$contao56File))
            return true;
            
        return false;
	}
	
	public static function getInfo()
	{
        $data = file_get_contents(self::$contao56File);
        
        if (preg_match("~const VERSION = '(.*?)';~i", $data, $match))
        {
            return ['Version' => $match[1]];
        }
        return false;
	}
}
?>
