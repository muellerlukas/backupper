<?php
class wordpress
{
	public static function isAvailable()
	{
		return file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-includes/version.php');
	}
	
	public static function getInfo()
	{
		require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-includes/version.php');
		return ['Version' => $wp_version];
		$versions = ['core' => ['core' => [
			'Name' =>  get_bloginfo('name'),
			'Description' =>  get_bloginfo('description'),
			'Version' =>  get_bloginfo('version'),
		]], 'plugin' =>  [], 'theme' =>  []];
	}
}
?>