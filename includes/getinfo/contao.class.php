<?php

class contao
{
	private static $result = [];
	
	public static function isAvailable()
	{
		$contao4File = $_SERVER['DOCUMENT_ROOT'] . '/../vendor/composer/installed.json';
		if (file_exists($contao4File))
		{
			$data = json_decode(file_get_contents($contao4File), 1);
			foreach($data as $package)
			{
				if($package['name'] == 'contao/core-bundle')
				{
					self::$result = ['Version' => $package['version']];
					return true;
				}
			}
		}
		
		$contao3File = $_SERVER['DOCUMENT_ROOT'] . '/system/config/constants.php';
		if (file_exists($contao3File))
		{
			require_once($contao3File);
			self::$result = ['Version' => VERSION . '.' . BUILD, 'Lts' => LONG_TERM_SUPPORT];
			return true;
		}
		return false;
	}
	
	public static function getInfo()
	{
		return self::$result;
	}
}