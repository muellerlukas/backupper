<?php

class openssl
{
	protected static $_cipher = 'AES-256-CBC';
	
	public static function encrypt($plaintext, $key)
	{
		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::$_cipher));
		$ciphertextRaw = openssl_encrypt($plaintext, self::$_cipher, $key, 0, $iv);
		$ciphertext = base64_encode($iv . $ciphertextRaw);
		return $ciphertext;
	}
	
	public static function decrypt($ciphertext, $key)
	{
		$ciphertext = base64_decode($ciphertext);
		
		$ivlen = openssl_cipher_iv_length(self::$_cipher);
		$iv = substr($ciphertext, 0, $ivlen);
		$ciphertextRaw = substr($ciphertext, $ivlen);
		$plaintext = openssl_decrypt($ciphertextRaw, self::$_cipher, $key, 0, $iv);		
		return $plaintext;
	}
}