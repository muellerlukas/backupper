<?php
$baseurl = 'http://192.168.1.23/backup/';

// get object with public key
$pubkey = json_decode(file_get_contents($baseurl . '?action=getpub'));


$data = [
	'db_type' => 'mysql', // type of db, optional. Defaults to mysql. pgsql and mssql is planned
	'db_host' => '127.0.0.1', // hostname, optional. Defaults so 127.0.0.1
	'db_port' => '3306', // port, optional. Defaults so 4406
	'db_name' => '', // name of db
	'db_user' => '', // user that is allowed to read
	'db_password' => '', // password of that user
	'valid_until' => (time() + 60), // timestamp how long the link is valid
	'try_zip' => 0, // if zip-extension is installed zip the file
	'db_charset' => 'utf8mb4', // charset, optional. Defaults to utf8mb4
	'revision' => 0, // if true, a new file will be created. if false, the old one will be overwritten
];

openssl_public_encrypt(json_encode($data), $encdata, $pubkey->result);

$link = $baseurl . '?action=backup&data=' . urlencode(base64_encode($encdata)) . "\n";
echo $link;

/*
// Result when backup was successfull
// HTTP-Code: 2xx
{
    "error": 0,
    "result": {
        "usedMethod": "exec",
        "filename": "db\/127.0.0.1_d025ed3f.sql",
        "zipped": 0,
        "backupDuration": 1.0554730892181396484375,
        "md5": "a8d84c13fcba63f2740a236d6f109e8f",
        "ended": 1534243679
    }
}


// Result, when failed
// HTTP-Code: 4xx or 9xx (proprietary)
{
    "statuscode": 400,
    "result": {
        "errorHuman": "Could not decode data. Maybe wrong key?",
        "detailedError": "error:0406506C:rsa routines:RSA_EAY_PRIVATE_DECRYPT:data greater than mod len"
    }
}
*/