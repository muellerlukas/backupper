<?php
/* 
 * Copyright (c) 2018, Lukas Mueller (info@muellerlukas.de). All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
*/

// Composer Autoload
if (file_exists(__DIR__ . '/vendor/autoload.php'))
	require(__DIR__ . '/vendor/autoload.php');

if (!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'default';

require_once(__DIR__ . '/includes/openssl.class.php');

// default config, can be overwritten
$defaultConfig = [
	'revision' => 0,
	'try_zip' => 1,
	'db_type' => 'mysql',
	'db_host' => '127.0.0.1',
	'db_port' => 3306,
	'db_charset' => 'utf8mb4',
];


// default config, can be overwritten with own config.php
$fixedConfig = [
	'debug' => 1,
	'backup_dir' => __DIR__ . '/db',
	'backupFile' => '%dbhost%_%dbuser%_%dbname%.sql',
	'backupFileRev' => '%dbhost%_%dbuser%_%dbname%_%date%_%time%.sql',
	'pubKeys' => [],
	'limitByPubKey' => ['backup', 'getinfo'],
];

// load config, if exists
if (file_exists(__DIR__ . '/includes/config.inc.php'))
{
	// and merge with existing data
	$fixedConfig = array_merge($fixedConfig, require(__DIR__ . '/includes/config.inc.php'));
}
if ($fixedConfig['debug'])
	error_reporting(E_ALL);
else
	error_reporting(0);

//limit by key
if (!empty($fixedConfig['limitByPubKey']) && !empty($fixedConfig['pubKeys']))
{
	$sigError = true;
	
	
	if (empty($_REQUEST['valid_until']))
		$_REQUEST['valid_until'] = 0;
	
	if (is_array($fixedConfig['limitByPubKey']) && !in_array($_REQUEST['action'], $fixedConfig['limitByPubKey']))
	{
		$sigError = false;
	}
	elseif (!empty($_REQUEST['sig']))
	{
		foreach($fixedConfig['pubKeys'] as $pubkey)
		{

			$cryptinfo = openssl_verify($_REQUEST['action'] . ',' . $_REQUEST['valid_until']. ',' . $_REQUEST['cryptresult'] . ',' . $_SERVER['HTTP_HOST'], base64_decode($_REQUEST['sig']), $pubkey);
			openssl_error_string();
			if ($cryptinfo)
			{
				define('USEDPUBKEY', $pubkey);
				$sigError = false;
			}
		}
	}
	
	if ($sigError)
	{
		jsonexit(['errorHuman' => 'Invalid signature.'], 403);
	}
	elseif(!empty($_REQUEST['valid_until']) && $_REQUEST['valid_until'] <= time())
	{
		jsonexit(['errorHuman' => 'This link is no longer valid'], 410);
	}
}
// functions
function jsonexit($data, $statuscode = 200)
{
	global $defaultConfig, $fixedConfig;
	http_response_code($statuscode);
	$plaindata = json_encode(['statuscode' => $statuscode, 'result' => $data], JSON_PRETTY_PRINT);
	
	if (defined('USEDPUBKEY') && $_REQUEST['cryptresult'])
	{
		$password = md5(rand() . USEDPUBKEY);
		openssl_public_encrypt($password, $encdata, USEDPUBKEY);
		$result = ['crypted' => openssl::encrypt($plaindata, $password), 'password' => base64_encode($encdata)];
		die(json_encode($result));
	}
	die($plaindata);
}

$keyfile = __DIR__ . '/privkey.pem';

// create key, if not yet done
if (!file_exists($keyfile))
{
	// create and export private key
	$privkey = openssl_pkey_new();
	openssl_pkey_export($privkey, $exportpriv);
	if (!file_put_contents($keyfile, $exportpriv))
		jsonexit(['errorHuman' => 'Could not write private key'], 500);
}
else
{
	// load existing private key
	$privkey = file_get_contents($keyfile);
	if ($privkey === false)
		jsonexit(['errorHuman' => 'Could not read private key'], 500);
	
	$privkey = openssl_pkey_get_private($privkey);
}

if ($_REQUEST['action'] == 'getpub')
{
	$pubkey = openssl_pkey_get_details($privkey)["key"];
	openssl_pkey_free($privkey);
	jsonexit($pubkey);
}
elseif ($_REQUEST['action'] == 'getinfo')
{
	$result = [];
	foreach(glob(__DIR__ . '/includes/getinfo/*.class.php') as $infofile)
	{
		require_once($infofile);
		$class = basename($infofile, '.class.php');
		if($class::isAvailable())
		{
			$data = $class::getInfo();
			if ($data !== false)
                $result[$class] = $data;
        }
	}
	
	jsonexit($result);
}
elseif ($_REQUEST['action'] == 'selfupdate')
{
	$disabledFunctions = explode(',', ini_get('disable_functions'));
	if (!is_dir(__DIR__ . '/.git'))
	{
		jsonexit(['errorHuman' => 'No .git-directory found.'], 400);
	}
	elseif (in_array('exec', $disabledFunctions))
	{
		jsonexit(['errorHuman' => 'exec is disabled'], 400);
	}
	
	$lastoutput = exec('git pull', $output, $returncode);
	if ($returncode > 0)
	{
		jsonexit(['errorHuman' => 'Could not execute git.', 'detailedError' => $returncode], 900 + $returncode);
	}
	else
	{
		jsonexit(['result' => $output]);
	}
}
elseif($_REQUEST['action'] == 'backup')
{
	if (isset($_REQUEST['data']))
		$encdata = base64_decode($_REQUEST['data']);
	else
		$encdata = '';
	
	// Why the f*** does it need to call this? Else it throws afterwards an error. Oo
	openssl_error_string();
	// decrypt data
	openssl_private_decrypt($encdata, $jsondata, $privkey);
	openssl_pkey_free($privkey);
	$opensslError = openssl_error_string();
	
	#if($opensslError !== false)
	#	jsonexit(['errorHuman' => 'Could not decode data. Maybe wrong key?', 'detailedError' => $opensslError], 400);
	
	// coonvert json to array and merge with existing config
	$receivedConfig = json_decode($jsondata, 1);
	if (!is_array($receivedConfig))
		jsonexit(['errorHuman' => 'Invalid config or not successfully decrypted.'], 400);
	
	
	// Final config, each value is replaced with the following
	$config = array_merge($defaultConfig, $receivedConfig, $fixedConfig);

	// check if link is valid
	if(isset($config['valid_until']) && $config['valid_until'] < time())
		jsonexit(['errorHuman' => 'This link is no longer valid'], 410);
	
	// data to be replaced
	$fileReplacements = [
		'%dbhost%' => $config['db_host'],
		'%dbname%' => $config['db_name'],
		'%dbuser%' => $config['db_user'],
		'%date%' => date('Ymd'),
		'%time%' => date('His')
		];
	
	// prepare filename for backup
	$backupfile = $config['backup_dir'] . '/' . str_replace(array_keys($fileReplacements), $fileReplacements, ($config['revision'] ? $config['backupFileRev'] : $config['backupFile']));


	// we want to backup a mysql
	// TODO: Sanitzie input
	if ($config['db_type'] == 'mysql')
	{
		$pdoDSN = 'mysql:host=' . $config['db_host'] . ';port=' . (int)$config['db_port'] . ';dbname=' . $config['db_name'] . ';charset=' . $config['db_charset'];
		try
		{
			$pdo = new PDO($pdoDSN, $config['db_user'], $config['db_password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
		}
		catch(Exception  $e)
		{
			jsonexit(['errorHuman' => 'Could not connect to DB.', 'detailedError' => $e->getMessage()], 403);
		}
		
		$filezipped = 0;
		$disabledFunctions = explode(',', ini_get('disable_functions'));
		if (!in_array('exec', $disabledFunctions))
		{
			$exec = 'mysqldump';
			$exec .= ' -u' . escapeshellarg($config['db_user']);
			$exec .= ' -p' . escapeshellarg($config['db_password']);#
			$exec .= ' --single-transaction';
			$exec .= ' --hex-blob';
			$exec .= ' --extended-insert';
			$exec .= ' --routines --triggers';
			$exec .= ' ' . escapeshellarg($config['db_name']);
			
			// activate zipping
			// todo: check if gzip installed
			if ($config['try_zip'])
			{
				$backupfile .= '.zip';
				$exec .= ' | gzip > ' . $backupfile;
				$filezipped = 1;
			}
			else
			{
				$exec .= ' > ' . $backupfile;
			}

			// start backup
			$backupStart = microtime(1);
			
			$test = exec($exec, $output, $returncode);

			$backupDuration = microtime(1) - $backupStart;
			
			if ($returncode > 0)
			{
				jsonexit(['errorHuman' => 'Could not execute mysqldump.', 'detailedError' => $returncode], 900 + $returncode);
			}
			else
			{
				jsonexit(['usedMethod' => 'exec', 'filename' => $backupfile, 'zipped' => $filezipped, 'backupDuration' => $backupDuration, 'md5' => md5_file($backupfile), 'ended' => time()], 201);
			}
		}
		else // no exec privileges
		{
			if (file_exists(__DIR__ . '/includes/Mysqldump.php'))
				require_once(__DIR__ . '/includes/Mysqldump.php');
			
			$dumpsettings = [];
			try
			{
				if ($config['try_zip'])
				{
					$backupfile .= '.gz';
					$dumpsettings['compress'] = Ifsnop\Mysqldump\Mysqldump::GZIP;
					$filezipped = 1;
				}			
				$dump = new Ifsnop\Mysqldump\Mysqldump($pdoDSN, $config['db_user'], $config['db_password'], $dumpsettings);
				
				$backupStart = microtime(1);
				$dump->start($backupfile);
				$backupDuration = microtime(1) - $backupStart;
				
				jsonexit(['usedMethod' => 'php', 'filename' => $backupfile, 'zipped' => $filezipped, 'backupDuration' => $backupDuration, 'md5' => md5_file($backupfile), 'ended' => time()], 201);
			} catch(Exception $e)
			{
				jsonexit(['errorHuman' => 'Could not backup DB.', 'detailedError' => $e->getMessage()], 900);
			}
		}
	}
}
